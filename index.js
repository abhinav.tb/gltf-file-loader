
const express = require('express');



const app = express();
const port = process.env.PORT | 8000;

app.set('view engine', 'ejs');
app.use("/assets", express.static(__dirname + "/assets"));
app.use("/assets/house", express.static(__dirname + "/assets/house"));
app.use("/assets/js", express.static(__dirname + "/assets/js"));


app.get('/', function(req, res) {
	res.render('index');
})

app.listen(port, () => { console.log("Server started on port: " + port) });
